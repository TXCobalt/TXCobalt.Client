﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

using TXCobalt.Core;

namespace TXCobalt.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "TXCobalt client emulator";
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                Console.WriteLine("Connection ...");
                client.Connect(IPAddress.Parse(args[0]), int.Parse(args[1]));
                Console.WriteLine("Connection successful");
                Console.WriteLine("Get server response ...");
                byte[] buffer = new byte[client.ReceiveBufferSize];
                int lenght = client.Receive(buffer);

                List<byte> bufferl = new List<byte>(buffer);

                ServerResponse response = ServerResponse.Deserialize(bufferl.GetRange(0, lenght).ToArray());
                if (response.IsAvailable)
                    Console.WriteLine("Server is availlable !");
                else
                    Console.WriteLine("Server is not availlable");

                Console.WriteLine("{0}: {1}", "MapName", response.Map);
                Console.WriteLine("{0}: {1}", "PlayerCount", response.PlayerCount);
                Console.WriteLine("{0}: {1}", "MaxPlayer", response.MaxPlayer);
                Console.WriteLine("{0}: {1}", "IsModded", response.Moded);
                Console.WriteLine("{0}: {1}", "MOTD", response.MOTD);
                Console.WriteLine("{0}: {1}", "Need Password", response.PasswordProtected);
                Console.WriteLine("{0}: {1}", "Version", response.ProtocolVersion);
                Console.WriteLine("{0}: {1}", "Use alternative serialization algoritm", response.UseAlternateSerialization);

                if (response.IsAvailable)
                {
                    Console.WriteLine("\nTry to connect ...");
                    client.Send(Serializer.Serialize(new ConnectionRequest() { color = new Color(240, 255, 255), Password = null, Username = "TSnake41", Version = "dev-0.1" }));
                    Console.WriteLine("Connection success !");
                    Console.WriteLine("Recive data ...");
                    while(client.Connected)
                    {
                        buffer = new byte[4096];
                        if (client.Receive(buffer) == 0)
                            break;
                        Console.WriteLine("Packet recived !");
                        GameUpdate update = Serializer.Deserialize<GameUpdate>(buffer);

                        List<GameObjectContainer> containers = new List<GameObjectContainer>(update.gameobjects);
                        List<PlayerData> Players = new List<PlayerData>(update.Players);

                        containers.ForEach((GameObjectContainer container) =>
                        {
                            Console.WriteLine("Entity {0}, EntityID: {1} Pos: {2}, Tilt: {3}", container.Guid, container.TypeId, container.transform.Pos, container.transform.tilt);
                        });
                        Console.WriteLine("-------------");
                        Players.ForEach((PlayerData player) =>
                        {
                            Console.WriteLine("Username: {0}, GUID: {1}, Color: #{2}", player.Username, player.GUID, player.argbcolor);
                        });

                        Console.WriteLine("\nEnd of update\n");

                        Console.WriteLine("\nSend own packet.");
                        client.Send(Serializer.Serialize(new InputData() { Direction = Tilt.Right, IsFire = false, IsMove = true }));
                    }
                    client.Close();
                }
                Console.WriteLine("\n");
                client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured.");
                Console.WriteLine(e.ToString());
            }
            Console.ReadKey();
        }
    }
}
